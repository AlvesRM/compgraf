import * as THREE from 'three'
import { FBXLoader } from 'FBXLoader'
import { PointerLockControls } from 'PointerLockControls';

document.addEventListener('DOMContentLoaded',Start);

var cena = new THREE.Scene();
var camera = new THREE.OrthographicCamera(-1,3,1,-1,-10,10); //Adicionar Alteração
var renderer = new THREE.WebGLRenderer();

const near = 0.1; // Near plane distance
const fov = 45; // Field of view in degrees
const aspectRatio = 4 / 3; // Aspect ratio (width / height)
var cameraPerspetivaInit = new THREE.PerspectiveCamera(fov,aspectRatio,near,100)
var cameraPerspetiva = cameraPerspetivaInit

const frustumHeight = 2 * Math.tan(THREE.MathUtils.degToRad(fov / 2)) * near;
const frustumWidth = frustumHeight * aspectRatio;

var objetoImportado;
var bigLamp;
var mixerAnimacao;
var relogio = new THREE.Clock();
var importer = new FBXLoader();

renderer.setSize(window.innerWidth-15,window.innerHeight-60);
renderer.setClearColor(0xaaaaaa);

document.body.appendChild(renderer.domElement);

// T E S T
var redMarker = new THREE.BoxGeometry(0.5, 0.5, 0.5);
var bigRed = new THREE.MeshStandardMaterial(( {color: 0xFF0000,  opacity:0.5, transparent:true} ));
var originRed = new THREE.Mesh(redMarker, bigRed);

originRed.position.set(frustumWidth, frustumHeight, near * 2)

cameraPerspetiva.add(originRed)

//UTILIZAR A CAIXA A VOLTA DA CAMARA COMO COLISION BOX EM VEZ DE VERIFICAR AS COORDENADAS DA CAMERA

var previousCameraPosition = new THREE.Vector3();
previousCameraPosition.copy(cameraPerspetiva.position);

const controls = new PointerLockControls(cameraPerspetiva, renderer.domElement);
controls.addEventListener('lock', function () {
});

controls.addEventListener('unlock', function () {
});

document.addEventListener(
  'click',
  function () {
    controls.lock();
  },
  false
);

document.addEventListener("keydown", onDocumentKeyDown, false);
document.addEventListener("keyup", onDocumentKeyUp, false);

var movementSpeed = 4.5; 
var movementSpeedCap = 6;
var clock = new THREE.Clock();

var movementAllowed = { x: true, y: true, z: true };
var previousCameraPosition = new THREE.Vector3();
var newCameraPosition = new THREE.Vector3(); 
var ismoving = false
var updown = 1;
var perspCheck = true;

//Movimentos e colisão (wip)

function updateMovement() {
  const delta = clock.getDelta();
  const distance = movementSpeed * delta;

  var isCollision = false;
  originRed.position.copy(cameraPerspetiva.position);

  if (controls.isLocked) {
    if (keyState[87] || keyState[38]) { // W or Up arrow
      controls.moveForward(distance);
    } else if (keyState[83] || keyState[40]) { // S or Down arrow
      controls.moveForward(-distance);
    }
    if (keyState[65] || keyState[37]) { // A or Left arrow
      controls.moveRight(-distance);
    } else if (keyState[68] || keyState[39]) { // D or Right arrow
      controls.moveRight(distance);
    }
    if (keyState[67]) { // Crouch button
      if (!ismoving) {
        ismoving = true;
        
        const targetY = cameraPerspetiva.position.y - updown; 
        movementSpeedCap = movementSpeedCap - (3.0*updown)
        movementSpeed = movementSpeed - (3.0*updown)
        
        const duration = 0.5; 
        const startTime = Date.now(); 
        
        function updateCameraPosition() {
          const currentTime = Date.now(); 
          const elapsedTime = (currentTime - startTime) / 1000;
          const t = Math.min(elapsedTime / duration, 1);
          cameraPerspetiva.position.y = cameraPerspetiva.position.y + (targetY - cameraPerspetiva.position.y) * t;
          
          if (t < 1) {
            requestAnimationFrame(updateCameraPosition);
          } else {
            ismoving = false;
            updown = -updown
          }
        }
        requestAnimationFrame(updateCameraPosition);
      }
    }
    /*if (keyState[67]){
      if(perspCheck == true){
        cameraPerspetivaInit = cameraPerspetiva
        cameraPerspetiva = camera
        perspCheck = false
      }
      else if (perspCheck == false){
        camera = cameraPerspetiva
        cameraPerspetiva = cameraPerspetivaInit
        perspCheck = true
      }
    }*/
  }

  var isCollision = checkCollision();

  if (isCollision) {
    var restrictedPosition = cameraPerspetiva.position.clone(); // Create a clone of the current camera position
  
    if (movementAllowed.x) {
      restrictedPosition.x = previousCameraPosition.x; // Update the restricted position for the x-axis
      movementAllowed.x = false;
    }
    if (movementAllowed.y) {
      restrictedPosition.y = previousCameraPosition.y; // Update the restricted position for the y-axis
      movementAllowed.y = false;
    }
    if (movementAllowed.z) {
      restrictedPosition.z = previousCameraPosition.z; // Update the restricted position for the z-axis
      movementAllowed.z = false;
    }
  
    // Check if the restricted position is different from the current position
    if (!cameraPerspetiva.position.equals(restrictedPosition)) {
      cameraPerspetiva.position.copy(restrictedPosition); // Update the camera position
    }
  } else {
    previousCameraPosition.copy(cameraPerspetiva.position); // Update previousCameraPosition after successful movement
  }
}

function checkCollision() {
  const originRedBoundingBox = new THREE.Box3().setFromObject(originRed);

  for (let i = 0; i < walls.length; i++) {
    const wallBoundingBox = new THREE.Box3().setFromObject(walls[i]);

    if (wallBoundingBox.intersectsBox(originRedBoundingBox)) {
      const deltaCam = newCameraPosition.copy(cameraPerspetiva.position);

      if (deltaCam.x > 0) {
        movementAllowed.x = false;
        console.log(deltaCam.x + "x>0")
      } else if (deltaCam.x < 0) {
        movementAllowed.x = false;
        console.log(deltaCam.x + "x<0")
      }
      if (deltaCam.y > 0) {
        movementAllowed.y = false;
      } else if (deltaCam.y < 0) {
        movementAllowed.y = false;
      }
      if (deltaCam.z > 0) {
        movementAllowed.z = false;
        console.log(deltaCam.z + "z>0")
      } else if (deltaCam.z < 0) {
        movementAllowed.z = false;
        console.log(deltaCam.z + "z<0")
      }
    
      return true; // Collision detected
    }
  }

  return false; // No collision
}

var keyState = {};

function onDocumentKeyDown(event) {
  keyState[event.keyCode || event.which] = true;
  movementSpeed += 0.5;
  movementSpeed = Math.min(movementSpeed, movementSpeedCap);
}

function onDocumentKeyUp(event) {
  keyState[event.keyCode || event.which] = false;
  movementSpeed = 4.5;
}

const wallThickness = 1/3
const RoomX = 20 + wallThickness*3
const RoomY = 5
const RoomZ = 20 + wallThickness*3

const originX = - (RoomX/2)
const originZ = - (RoomZ/2)

//walls

const sizeIntro =       10      +wallThickness
const introWallX =      2.5     +wallThickness*0.5
const introWallZ =      10      - sizeIntro/2 + wallThickness

const sizeBig =         16.5    +wallThickness * 2.0
const bigWallX =        sizeBig/2
const bigWallZ =        12.5    +wallThickness * 0.5

const sizeBaby =        1       + wallThickness
const babyWallX =       RoomX   - (sizeBaby/2)
const babyWallZ  =      12.5    +wallThickness * 0.5

const sizeHalf =        10
const halfWallX =       16.5    +wallThickness * 2.5 
const halfWallZ =       sizeHalf/2
const sizeBot =         7.5
const sizeKit =         3.5     +wallThickness

const sizeBath  =       6.5     +wallThickness
const bathWallX =       sizeBath/2 + 10 + wallThickness
const bathWallZ =       15      +wallThickness

const sizeBed  =        5.5       
const bedWallX =        10      +wallThickness * 1.5
const bedWallZ =        RoomZ   - sizeBed*1.5

//Testing (em principio vamos ter que queriar um novo para cada com um UV map proprio)
var wallWidth = sizeBaby
var wallHeight = RoomY

var textura = new THREE.TextureLoader().load('./skybox/brickwall.png', function (textura){
  textura.wrapS = textura.wrapT = THREE.RepeatWrapping;
  textura.repeat.set(3*(wallWidth/RoomX),(wallHeight/RoomY))
});
var materialTextura = new THREE.MeshStandardMaterial({map : textura});

var textura2 = new THREE.TextureLoader().load('./skybox/ceiling.png', function (textura){
  textura2.wrapS = textura2.wrapT = THREE.RepeatWrapping;
  textura2.repeat.set(3*(wallWidth/RoomX),(wallHeight/RoomY))
});
var ceilingTextura = new THREE.MeshStandardMaterial({map : textura2});

var textura3 = new THREE.TextureLoader().load('./skybox/floor.png', function (textura){
  textura3.wrapS = textura3.wrapT = THREE.RepeatWrapping;
  textura3.repeat.set(3*(wallWidth/RoomX),(wallHeight/RoomY))
});
var floorTextura = new THREE.MeshStandardMaterial({map : textura3});

var geometriaIntroWall = new THREE.BoxGeometry(wallThickness,RoomY, sizeIntro);
var meshIntroWall = new THREE.Mesh(geometriaIntroWall, materialTextura);

var geometriaBigWall = new THREE.BoxGeometry(sizeBig,RoomY, wallThickness);
var meshBigWall = new THREE.Mesh(geometriaBigWall, materialTextura);

var geometriaBabyWall = new THREE.BoxGeometry(sizeBaby,RoomY, wallThickness);
var meshBabyWall = new THREE.Mesh(geometriaBabyWall, materialTextura);
var meshBabyWall2 = new THREE.Mesh(geometriaBabyWall, materialTextura);

var geometriaHalfWall = new THREE.BoxGeometry(wallThickness,0.75, sizeHalf);
var geometriaHalfWallBot = new THREE.BoxGeometry(wallThickness,2, sizeBot);
var meshHalfWall = new THREE.Mesh(geometriaHalfWall, materialTextura);
var meshHalfWallBot = new THREE.Mesh(geometriaHalfWallBot, materialTextura);

var geometriaKitWall = new THREE.BoxGeometry(sizeKit, RoomY, wallThickness);
var meshKitWall = new THREE.Mesh(geometriaKitWall, materialTextura);

var geometriaBathWall = new THREE.BoxGeometry(sizeBath, RoomY, wallThickness);
var meshBathWall = new THREE.Mesh(geometriaBathWall, materialTextura);

var geometriaBedWall = new THREE.BoxGeometry(wallThickness, RoomY, sizeBed);
var meshBedWall = new THREE.Mesh(geometriaBedWall, materialTextura);

//

//Imagens 
var textura1 = new THREE.TextureLoader().load('./Imagens/Sofá.jpg');
var textura2 = new THREE.TextureLoader().load('./Imagens/Madeira.jpg');
var textura3 = new THREE.TextureLoader().load('./Imagens/Cadeiras.jpg');
var textura4 = new THREE.TextureLoader().load('./Imagens/Pedra.jpg');
var textura5 = new THREE.TextureLoader().load('./Imagens/Madeira2.jpg');
var textura6 = new THREE.TextureLoader().load('./Imagens/Metais.jpg');
var textura7 = new THREE.TextureLoader().load('./Imagens/Pillow.jpg');

//Por a imagem em forma que de para utilizar nas BoxGeometry()
var materialCubo1 = new THREE.MeshStandardMaterial( { map:textura1} ); 
var materialCubo2 = new THREE.MeshStandardMaterial( { map:textura2} ); 
var materialCubo3 = new THREE.MeshStandardMaterial( { map:textura3} ); 
var materialCubo4 = new THREE.MeshStandardMaterial( { map:textura4} ); 
var materialCubo5 = new THREE.MeshStandardMaterial( { map:textura5} ); 
var materialCubo6 = new THREE.MeshStandardMaterial( { map:textura6} ); 
var materialCubo7 = new THREE.MeshStandardMaterial( { map:textura7} ); 

//Not-Sky-Box

///MAKE ALL THE WALLS AGAIN

var roomFloorCeiling = new THREE.BoxGeometry(RoomX+wallThickness*2,0.05,RoomZ+wallThickness*2)
var roomFrontBackGeometry = new THREE.BoxGeometry(wallThickness,RoomY,RoomZ )
var roomLeftGeometry = new THREE.BoxGeometry(RoomX,RoomY,wallThickness)

var RoomWallFront = new THREE.Mesh(roomFrontBackGeometry,materialTextura)
var RoomWallBack  = new THREE.Mesh(roomFrontBackGeometry,materialTextura)
var RoomLeftWall = new THREE.Mesh(roomLeftGeometry, materialTextura)
RoomWallFront.position.set(RoomX/2+wallThickness/2,RoomY/2,0)
RoomWallBack.position.set(-RoomX/2-wallThickness/2,RoomY/2,0)
RoomLeftWall.position.set(0,RoomY/2,RoomZ/2+wallThickness/2)

var roomFinal = new THREE.Mesh(roomFloorCeiling,floorTextura)
var roomCeiling = new THREE.Mesh(roomFloorCeiling,ceilingTextura)

var roomSidebits = new THREE.BoxGeometry(RoomX*(2/5),RoomY,wallThickness)
var roomTopBit = new THREE.BoxGeometry(RoomX*(1/5),0.5,wallThickness)
var roomWindowL = new THREE.Mesh(roomSidebits,materialTextura)
var roomWindowR  = new THREE.Mesh(roomSidebits,materialTextura)
var roomWindowT = new THREE.Mesh(roomTopBit, materialCubo6)

roomWindowT.position.set(RoomX*(3/10),RoomY/2-0.25,0)
roomWindowR.position.set(RoomX*(3/5),0,0)
roomWindowL.add(roomWindowT)
roomWindowL.add(roomWindowR)
roomWindowL.position.set(-6.3,RoomY/2,-RoomZ/2-wallThickness/2)

roomCeiling.position.set(0,RoomY,0)
roomFinal.add(roomCeiling)
roomFinal.add(RoomWallFront)
roomFinal.add(RoomWallBack)
roomFinal.add(RoomLeftWall)
roomFinal.add(roomWindowL)
roomFinal.position.set(0,-RoomY/2-(0.05/2),0)

var walls = []

//Not-Sky-Box End

//X Walls
meshIntroWall.position.set(originX + introWallX, 0, originZ + introWallZ)
walls.push(meshIntroWall)

meshHalfWallBot.position.set(0,-3.625,-1.25)
meshKitWall.position.set(1.750,-(2.5-(0.75/2)),5+wallThickness/2)
meshHalfWall.add(meshHalfWallBot);
meshHalfWall.add(meshKitWall)
meshHalfWall.position.set(originX + halfWallX,2.5-(0.75/2), originZ + halfWallZ)
walls.push(meshHalfWall)

meshBedWall.position.set(originX + bedWallX,0,originZ+bedWallZ+sizeBed)
walls.push(meshBedWall)

//Z Walls
meshBigWall.position.set( originX + bigWallX, 0, originZ + bigWallZ)
walls.push(meshBigWall)
meshBabyWall.position.set(originX + babyWallX, 0, originZ + babyWallZ)
walls.push(meshBabyWall)
meshBabyWall2.position.set(originX + babyWallX, 0, originZ + babyWallZ + 2.5 )
walls.push(meshBabyWall2)
meshBathWall.position.set(originX + bathWallX,0, originZ + bathWallZ)
walls.push(meshBathWall)
//walls end

//Tentativa de implementar "colisões"

//OBJETO IMPORTADO = CEELING FAN
importer.load('./Objetos/Simple_Ceiling_Fan.fbx', function(object){
    mixerAnimacao = new THREE.AnimationMixer(object)

    //var action = mixerAnimacao.clipAction(object.animations[0]);
    //action.play();

    object.traverse(function (child){
        if(child.isMesh){
            child.castShadow = true;
            child.recieveShadow = true;
        }
    });
 
    cena.add(object);

    object.scale.x=0.02;
    object.scale.y=0.02;
    object.scale.z=0.02;

    object.position.x=0;
    object.position.y=1.5;
    object.position.z=-1;

    objetoImportado = object;
});



//OBJETO IMPORTADO = LAMP
importer.load('./Objetos/Lamp.fbx', function(object){
  mixerAnimacao = new THREE.AnimationMixer(object)

  //var action = mixerAnimacao.clipAction(object.animations[0]);
  //action.play();

  object.traverse(function (child){
      if(child.isMesh){
          child.castShadow = true;
          child.recieveShadow = true;
      }
  });
  
  object.position.set(0,0,0)

  var strongerlight = new THREE.PointLight("#F48020", 3, 6);
  strongerlight.position.set(0, 100, 0)
  strongerlight.castShadow = false
  object.add(strongerlight)

  var focoLuz = new THREE.PointLight(0xffffff, 0.1, 10);
  focoLuz.position.set(0,100,0)
  focoLuz.castShadow = true
  object.add(focoLuz)

  cena.add(object);

  const boundingBox = new THREE.Box3().setFromObject(object);
  const size = new THREE.Vector3();
  boundingBox.getSize(size);
  
  const scaleX = 1 / size.y;  // 1 unit high
  const scaleY = 0.75 / size.x;  // 0.75 units in length
  const scaleZ = 0.75 / size.z;  // 0.75 units in width

  object.scale.set(scaleX, scaleY, scaleZ);

  object.position.x=5.65;
  object.position.y=-2.5+1.5+0.2
  object.position.z=-6;
});

var spotLight = new THREE.AmbientLight(0xffffff,0.55);
spotLight.position.set(0,0,0);
spotLight.castShadow = true;
//spotLight.lookAt(Room)
//Luz END

//skybox
    var texture_dir = new THREE.TextureLoader().load('./skybox/stormydays_rt.png');
    var texture_esq = new THREE.TextureLoader().load('./skybox/stormydays_lf.png');
    var texture_up  = new THREE.TextureLoader().load('./skybox/stormydays_dn.png');
    var texture_dn  = new THREE.TextureLoader().load('./skybox/stormydays_up.png');
    var texture_bk  = new THREE.TextureLoader().load('./skybox/stormydays_bk.png');
    var texture_ft  = new THREE.TextureLoader().load('./skybox/stormydays_ft.png');  
    
    var materialOutArray = [];

    materialOutArray.push(new THREE.MeshStandardMaterial({map: texture_dir}));
    materialOutArray.push(new THREE.MeshStandardMaterial({map: texture_esq}));
    materialOutArray.push(new THREE.MeshStandardMaterial({map: texture_dn}));
    materialOutArray.push(new THREE.MeshStandardMaterial({map: texture_up}));
    materialOutArray.push(new THREE.MeshStandardMaterial({map: texture_bk}));
    materialOutArray.push(new THREE.MeshStandardMaterial({map: texture_ft}));

    for (var i = 0; i<6;i++){
        materialOutArray[i].side = THREE.BackSide;
    }

    var skyboxGeo = new THREE.BoxGeometry(50,10,50);

    var skybox = new THREE.Mesh(skyboxGeo,materialOutArray);
//skybox end

//TEST

var texturaWind = new THREE.TextureLoader().load('./skybox/glassfrosted.png');
const geometry = new THREE.BoxGeometry(RoomX*(1/5), 4.5, wallThickness);
const material = new THREE.MeshStandardMaterial({ map: texturaWind, transparent: true, opacity: 0.5});
const rectangle = new THREE.Mesh(geometry, material);
rectangle.position.set(0, -0.25, -RoomZ/2-wallThickness/2);
const light = new THREE.PointLight('#E7C65A', 1, 10);
light.position.set(0, 0, 1);
rectangle.add(light);

//MOBILIA # BANHEIRA

var geometriaCubo1B = new THREE.BoxGeometry(5.5, 0.25, 2.0);
var geometriaCubo2B = new THREE.BoxGeometry(5.5, 1.05, 0.25);
var geometriaCubo3B = new THREE.BoxGeometry(5.5, 1.05, 0.25);
var geometriaCubo4B = new THREE.BoxGeometry(0.25, 1.05, 1.5);
var geometriaCubo5B = new THREE.BoxGeometry(0.25, 1.05, 1.5);

var banheira = new THREE.Mesh(geometriaCubo1B, materialCubo4);
var banheirap1 = new THREE.Mesh(geometriaCubo2B, materialCubo4);
var banheirap2 = new THREE.Mesh(geometriaCubo3B, materialCubo4);
var banheirap3 = new THREE.Mesh(geometriaCubo4B, materialCubo4);
var banheirap4 = new THREE.Mesh(geometriaCubo5B, materialCubo4);

banheirap1.position.set(0, (0.25/2) + 1.05/2, 1.0-(0.25/2));
banheirap2.position.set(0, (0.25/2) + 1.05/2, -(1.0-(0.25/2)));
banheirap3.position.set(-((5.5/2)-(0.25/2)), (0.25/2) + 1.05/2, 0);
banheirap4.position.set((5.5/2)-(0.25/2), (0.25/2) + 1.05/2, 0);

banheira.add(banheirap1)
banheira.add(banheirap2)
banheira.add(banheirap3)
banheira.add(banheirap4)

banheira.position.set(3.35+wallThickness,-2.5,originZ + RoomZ - 1.5)

// MOBILIA # CADEIRAS

//Tamanho
//Cadeira
var geometriaCubo1C = new THREE.BoxGeometry(1.2, 1.7, 0.1);
var geometriaCubo2C = new THREE.BoxGeometry(1.2, 0.2, 0.8);
//Pernas da cadeira
var geometriaCubo3C = new THREE.BoxGeometry(0.12, 0.8, 0.12);

//Cadeira
var cadeira = new THREE.Mesh(geometriaCubo1C, materialCubo3);
var cadeirap2 = new THREE.Mesh(geometriaCubo2C, materialCubo3);

//Pernas da Cadeira
var cadeirap3 = new THREE.Mesh(geometriaCubo3C, materialCubo5);
var cadeirap4 = new THREE.Mesh(geometriaCubo3C, materialCubo5);
var cadeirap5 = new THREE.Mesh(geometriaCubo3C, materialCubo5);
var cadeirap6 = new THREE.Mesh(geometriaCubo3C, materialCubo5);

//Cadeira
cadeira.position.set( 0, 0, 0);
cadeirap2.position.set( 0, -(1.7/2)+0.1, 0.45);
//Pernas da cadeira 
cadeirap3.position.set(-(1.2-0.12)/2,-(1.7+0.8)/2,     (0.12-0.1)/2);
cadeirap4.position.set( (1.2-0.12)/2,-(1.7+0.8)/2,     (0.12-0.1)/2);
cadeirap5.position.set(-(1.2-0.12)/2,-(1.7+0.8)/2, 0.8-(0.12-0.1)/2);
cadeirap6.position.set( (1.2-0.12)/2,-(1.7+0.8)/2, 0.8-(0.12-0.1)/2);

cadeira.add(cadeirap2)
cadeira.add(cadeirap3)
cadeira.add(cadeirap4)
cadeira.add(cadeirap5)
cadeira.add(cadeirap6)

cadeira.position.set(5.65,-2.5 + 0.85 + 0.8,-8)
var cadeira2 = cadeira.clone();
cadeira2.rotateY(Math.PI)
cadeira2.position.set(5.65,-2.5 + 0.85 + 0.8,-4)

//MOBILIA # BALCAO

//Tamanho
//Balcão
var geometriaCubo1Ba = new THREE.BoxGeometry(2.0, 1.5, 0.8);
//Pedra do Balcão 
var geometriaCubo2Ba = new THREE.BoxGeometry(2.0, 0.1, 0.85);//Cima
var geometriaCubo3Ba = new THREE.BoxGeometry(1.95, 0.1, 0.75);//Baixo
//Porta do Balcão 
var geometriaCubo4Ba = new THREE.BoxGeometry(0.60, 1.40, 0.7);//Porta lado esquerdo 
var geometriaCubo5Ba = new THREE.BoxGeometry(0.60, 1.40, 0.7);//Porta Lado Direito

//Textura
//Balcão
var balcao = new THREE.Mesh(geometriaCubo1Ba, materialCubo5);
//Pedra do Balcão
var balcaop1 = new THREE.Mesh(geometriaCubo2Ba, materialCubo4);
var balcaop2 = new THREE.Mesh(geometriaCubo3Ba, materialCubo4);
//Portas do Balcão 
var balcaop3 = new THREE.Mesh(geometriaCubo4Ba, materialCubo2);
var balcaop4 = new THREE.Mesh(geometriaCubo5Ba, materialCubo2);

//Definir Posições
//Balcão
balcao.position.set(0, 0, 0);
//Pedra do Balcão
balcaop1.position.set(0, 0.8, 0.025);//Cima
balcaop2.position.set(0.0, -0.79, 0.0);//Baixo
//Portas do Balcão
balcaop3.position.set(-0.55, 0.003, 0.07);//Lado esquerdo
balcaop4.position.set(0.55, -0.003, 0.07);//Lado direito

balcao.add(balcaop1);
balcao.add(balcaop2);
balcao.add(balcaop3);
balcao.add(balcaop4);

balcao.rotateY(Math.PI*(-1/2))
balcao.position.set(originX+20+wallThickness*3-0.4,(-5+1.7)/2,originZ+1)
var balcao2 = balcao.clone();
balcao2.position.set(originX+20+wallThickness*3-0.4,(-5+1.7)/2,originZ+1+2.0)
var balcao3 = balcao.clone();
balcao3.position.set(originX+20+wallThickness*3-0.4,(-5+1.7)/2,originZ+1+2.0*2)
var balcao4 = balcao.clone();
balcao4.position.set(originX+20+wallThickness*3-0.4,(-5+1.7)/2,originZ+1+2.0*3)
var balcao5 = balcao.clone();
balcao5.position.set(originX+20+wallThickness*3-0.4,(-5+1.7)/2,originZ+1+2.0*4)

//MOBILIA # CAMA
//Tamanho
//Cama
var geometriaCubo1Ca = new THREE.BoxGeometry(3, 4, 0.1);
var geometriaCubo2Ca = new THREE.BoxGeometry(0.7, 0.2, 0.4);
var geometriaCubo3Ca = new THREE.BoxGeometry(2.8, 0.7, 4);
//Pernas da cama 
var geometriaCubo4Ca = new THREE.BoxGeometry(2.9, 0.8, 4.05);

//Textura 
var cama = new THREE.Mesh(geometriaCubo1Ca, materialCubo2);
var camap1 = new THREE.Mesh(geometriaCubo2Ca, materialCubo7);
var camap2 = new THREE.Mesh(geometriaCubo2Ca, materialCubo7);
var camap3 = new THREE.Mesh(geometriaCubo3Ca, materialCubo1);
var camap4 = new THREE.Mesh(geometriaCubo4Ca, materialCubo2);

//Definir Posições
//Cama 
cama.position.set(0, 0, 0);
camap1.position.set(-0.55,  -2+0.8+0.7+0.1, (0.1+0.4)/2);
camap2.position.set( 0.55,  -2+0.8+0.7+0.1, (0.1+0.4)/2);
camap3.position.set( 0,   -2+0.8+0.35, 2.05);
//Pernas da cama 
camap4.position.set(0,-2+(0.8/2),2.075);

cama.add(camap1)
cama.add(camap2)
cama.add(camap3)
cama.add(camap4)

cama.rotateY(Math.PI*(-1/2))
cama.position.set(originX + 10 + wallThickness - 0.05, -2.5 + 2, originZ+RoomZ-3)

//MOBILIA # LAVATORIO

//Lavatório
var geometriaCubo1L = new THREE.BoxGeometry(2.0, 1.5, 0.8);
//Pedra do lavatorio 
var geometriaCubo2L = new THREE.BoxGeometry(2.2, 0.1, 1.0);//Cima
var geometriaCubo3L = new THREE.BoxGeometry(1.95, 0.1, 0.75);//Baixo
//Porta do lavatorio 
var geometriaCubo4L = new THREE.BoxGeometry(0.60, 1.40, 0.7);//Porta lado esquerdo 

var geometriaCubo5L = new THREE.BoxGeometry(1.0, 0.1, 0.6);//Lavatório
var geometriaCubo6L = new THREE.BoxGeometry(0.04, 0.5, 0.08);//Torneira
var geometriaCubo7L = new THREE.BoxGeometry(0.04, 0.05, 0.15);
var geometriaCubo8L = new THREE.BoxGeometry(0.04, 0.04, 0.04);

//Textura
//Lavatorio
var lavatorio = new THREE.Mesh(geometriaCubo1L, materialCubo5);
//Pedra do Lavatorio
var lavatoriop1 = new THREE.Mesh(geometriaCubo2L, materialCubo4);
var lavatoriop2 = new THREE.Mesh(geometriaCubo3L, materialCubo4);
//Portas do Lavatorio 
var lavatoriop3 = new THREE.Mesh(geometriaCubo4L, materialCubo2);
var lavatoriop4 = new THREE.Mesh(geometriaCubo4L, materialCubo2);
//Lavatório
var lavatoriop5 = new THREE.Mesh(geometriaCubo5L, materialCubo6);
//Torneira
var lavatoriop6 = new THREE.Mesh(geometriaCubo6L, materialCubo6);
var lavatoriop7 = new THREE.Mesh(geometriaCubo7L, materialCubo6);
var lavatoriop8 = new THREE.Mesh(geometriaCubo8L, materialCubo6);
var lavatoriop9 = new THREE.Mesh(geometriaCubo8L,materialCubo6);

//Definir Posições
//Lavatorio
lavatorio.position.set(0, 0, 0);
//Pedra do Lavatorio
lavatoriop1.position.set(0, 0.8, 0);//Cima
lavatoriop2.position.set(0.0, -0.79, 0.0);//Baixo
//Portas do Lavatorio
lavatoriop3.position.set(-0.55, 0.003, 0.07);//Lado esquerdo
lavatoriop4.position.set(0.55, -0.003, 0.07);//Lado direito
//Lavatorio
lavatoriop5.position.set( 0, 0.9, 0.01);//pia
lavatoriop6.position.set( 0, 1.12, 0.025-0.2);//Torneira V
lavatoriop7.position.set(0, 1.25, 0.1-0.2);//Torneira H
lavatoriop8.position.set(0.15, 0.97, 0.025-0.2);//T1
lavatoriop9.position.set(-0.15, 0.97, 0.025-0.2);

lavatorio.add(lavatoriop1)
lavatorio.add(lavatoriop2)
lavatorio.add(lavatoriop3)
lavatorio.add(lavatoriop4)
lavatorio.add(lavatoriop5)
lavatorio.add(lavatoriop6)
lavatorio.add(lavatoriop7)
lavatorio.add(lavatoriop8)
lavatorio.add(lavatoriop9)

lavatorio.position.set(originX+RoomX-1.1,-2.5+(1.5/2),originZ+RoomZ-0.5)
lavatorio.rotateY(Math.PI)

//MOBILIA # SOFA

//Mesa
var geometriaCubo1S = new THREE.BoxGeometry(2.0, 0.2, 1.5);
//Pernas da Mesa
var geometriaCubo2S = new THREE.BoxGeometry(0.15, 1.5, 0.15);

//Textura
//Mesa
var mesa = new THREE.Mesh(geometriaCubo1S, materialCubo5);

//Pernas da Mesa
var mesap1 = new THREE.Mesh(geometriaCubo2S, materialCubo5);
var mesap2 = new THREE.Mesh(geometriaCubo2S, materialCubo5);
var mesap3 = new THREE.Mesh(geometriaCubo2S, materialCubo5);
var mesap4 = new THREE.Mesh(geometriaCubo2S, materialCubo5);

//Definir Posições
//Mesa
mesa.position.set( 0, 0, 0);
//Pernas da Mesa
mesap1.position.set(-0.9,-0.1-0.7, 0.65);
mesap2.position.set( 0.9,-0.80, 0.65);
mesap3.position.set( 0.9,-0.80,-0.65);
mesap4.position.set(-0.9,-0.80,-0.65);

mesa.add(mesap1)
mesa.add(mesap2)
mesa.add(mesap3)
mesa.add(mesap4)

mesa.position.set(5.65,-2.5+0.1+1.5,-6)

//MOBILIA # SOFA

//Sofá
var geometriaCubo1So = new THREE.BoxGeometry(3.4, 2.0, 0.3);  //Costas
var geometriaCubo2So = new THREE.BoxGeometry(0.4, 0.5, 0.8); //Braçoes
var geometriaCubo3So = new THREE.BoxGeometry(3.2, 0.6, 1.2); //Rabo
//Pernas do sofá
var geometriaCubo4So = new THREE.BoxGeometry(3.15, 0.25, 1.15);

//Sofá
var sofa = new THREE.Mesh(geometriaCubo1So, materialCubo1);
var sofap1 = new THREE.Mesh(geometriaCubo2So, materialCubo1);
var sofap2 = new THREE.Mesh(geometriaCubo2So, materialCubo1);
var sofap3 = new THREE.Mesh(geometriaCubo3So, materialCubo1);

//Pernas do sofá
var sofap4 = new THREE.Mesh(geometriaCubo4So, materialCubo2);

//Definir Posições
//Sofa
sofa.position.set(0, 0, 0);
sofap1.position.set(-2+(0.4/2),-1.0+(0.55), (0.3+0.8)/2);
sofap2.position.set( 2-(0.4/2),-1.0+(0.55), (0.3+0.8)/2);
sofap3.position.set(0,-1.0+0.3,(0.6+0.7)/2);
//Pernas do sofá
sofap4.position.set(-0,-1.0-(0.25/2),0.45);

sofa.add(sofap1)
sofa.add(sofap2)
sofa.add(sofap3)
sofa.add(sofap4)

sofa.rotateY(Math.PI*(1/2))
sofa.position.set(originX+2.5+wallThickness+0.2,-2.5+1.1+0.25/2,originZ+6)

function Start(){
    cena.add(skybox)

    //cena.add(focoLuz)
    cena.add(spotLight);
    //cena.add(strongerlight)
    cena.add(rectangle)

    cena.add(originRed)
    cena.add(roomFinal)

    cena.add(meshIntroWall);
    cena.add(meshHalfWall);
    cena.add(meshBigWall)
    cena.add(meshBabyWall)
    cena.add(meshBabyWall2)
    cena.add(meshBathWall)
    cena.add(meshBedWall)

    cena.add(banheira)
    cena.add(cadeira)
    cena.add(cadeira2)
    cena.add(balcao)
    cena.add(balcao2)
    cena.add(balcao3)
    cena.add(balcao4)
    cena.add(balcao5)
    cena.add(cama)
    cena.add(lavatorio)
    cena.add(mesa)
    cena.add(sofa)

    renderer.render(cena, cameraPerspetiva);
    requestAnimationFrame(loop);
}

function loop(){
    if(objetoImportado)
        objetoImportado.rotation.y += Math.PI/180*1

    /*if(mixerAnimacao){
        mixerAnimacao.update(relogio.getDelta());
    }*/

    updateMovement();
    renderer.render(cena,cameraPerspetiva);

    requestAnimationFrame(loop);
}